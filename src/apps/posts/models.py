from django.conf import settings

from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.utils import timezone

from django.utils.text import slugify

# Create your models here.
#MVC

# Post.objects.all()
# Post.objects.create(user=user, title="Some title")

class PostManager(models.Manager):
    def active(self, *args, **kwargs):
        #Post.objects.all() = super(PostManager, self).all()
        return super(PostManager, self).filter(draft=False).filter(publish__lte=timezone.now())

def upload_location(instance, filename):
    #filebase, extension = filename.split(".")
    #return "%s/%s.%s" %(instance.id, instance.id, extension)
    return "%s/%s" %(instance.id, filename)

#class Category(models.Model):
#    name = models.CharField(max_length=60)
#
#    class Meta:
#        verbose_name_plural = 'Categorias'
#
#    def __str__(self):
#        return self.name

class Post(models.Model):
#    category     = models.ForeignKey(Category)
    user         = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title        = models.CharField('Título', max_length=120)
    slug         = models.SlugField(unique=True)
    image        = models.ImageField(upload_to=upload_location,
                        null=True,
                        blank=True,
                        width_field="width_field",
                        height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field  = models.IntegerField(default=0)
    content      = models.TextField('Conteúdo')
    draft        = models.BooleanField(default=False)
    publish      = models.DateField(auto_now=False, auto_now_add=False) # Quando nós adicionamos um outro atributo a tabela posteriormente a mesma ja ter sido criada, vai dar um aviso em nosso makemigrations por não saber qual valor colocar nos dados já existentes, isso ocorre por não existir um default aqui
    timestamp    = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated      = models.DateTimeField(auto_now=True, auto_now_add=False)

    objects = PostManager()

    def __str__(self): # In case of Python 2 I use __unicode__
        return self.title

    def get_absolute_url(self):
        return reverse("posts:detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-timestamp", "-updated"]

def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Post.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

    '''slug = slugify(instance.title)
    # Se o título for "Tesla item 1" -> "tesla-item-1"
    exists = Post.objects.filter(slug=slug).exists()
    if exists:
        slug = "%s-%s" %(slug, instance.id)
    instance.slug=slug'''

pre_save.connect(pre_save_post_receiver, sender=Post)
