from django.contrib import admin

# Register your models here.
from .models import Post#, Category # colocando o .models ao invés de posts.models ele vai pegar o modelo que contém na pasta. Melhor, pois, se eu mudar o nome da app depois

#@admin.register(Category)
#class CategoryModelAdmin(admin.ModelAdmin):
#    list_display = ['name']

@admin.register(Post)
class PostModelAdmin(admin.ModelAdmin):
    list_display       = ['title', 'updated', 'timestamp']
    list_display_links = ['updated']
    list_editable      = ['title']
    list_filter        = ['updated', 'timestamp']
    search_fields      = ['title']
    save_on_top        = True
    class Meta:
        model = Post
